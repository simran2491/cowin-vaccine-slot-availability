import smtplib
import config
import json

from email.mime.text import MIMEText


def send_mail(data):
    #pdb.set_trace()
    body = json.dumps(data,indent=2)
    sent_from = config.EMAIL_USER
    subject = 'OMG Super Important Message'

    msg = MIMEText(str(body))
    msg['Subject'] = '!!! IMPORTANT..Vaccine Availability Alert !!!'
    msg['From'] = sent_from
    msg['To'] = ','.join(config.TO_USERS)
    msg['CC'] = ','.join(config.CC_USERS)
    rcpt = config.TO_USERS + config.CC_USERS
    server = smtplib.SMTP('smtp-mail.outlook.com', 587)
    server.ehlo()
    server.starttls()
    server.login(config.EMAIL_USER, config.EMAIL_PASSWORD)
    server.sendmail(sent_from, rcpt, msg.as_string())
    server.close()
    config.logger.info('Email sent!')
