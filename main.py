import argparse
import time
import config
from vaccine import VaccineSlots


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--age", "-a", help="set age", default=18, type=int)
    parser.add_argument("--district", "-d", help="set district", type=str, default='141,145,140,146,147,143,148,149,144,150,142')
    parser.add_argument("--pincode", "-p", help="set pincode", type=str, default='110008')
    args = parser.parse_args()
    slot = VaccineSlots(args.age, args.district.split(','),args.pincode.split(','))
    config.logger.info('Checking..')
    slot.get_vaccine_slots()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass
